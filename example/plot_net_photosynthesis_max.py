"""
Assimilation max response to temperature
========================================

Plot response to temperature of assimilation max curves to compare to fig3.
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.optimize import least_squares

from greer2018 import pth_clean

# read data
df = pd.read_csv(pth_clean / "net_photosynthesis_max.csv", sep=";", comment="#")

# fit curves for co2 = 300 [µmol mol-1] modality
meas = df[df['co2'] == 300]


def crv(x, params):
    return sum(coeff * pow(x, n) for n, coeff in enumerate(params))


def err(params, vname):
    x = meas['temp']
    sim = crv(x, params)
    obs = meas[vname]

    return sim - obs


res = least_squares(err, x0=[1, 1, 1], args=('amax',))
opt_amax = res.x
print("amax", repr(opt_amax))

res = least_squares(err, x0=[1, 1, 1], args=('ppfd_sat',))
opt_ppfd_sat = res.x
print("ppfd_sat", repr(opt_ppfd_sat))

# plot data
fig, axes = plt.subplots(2, 1, sharex='all', figsize=(10, 6), squeeze=False)

ax = axes[0, 0]
for co2, sdf in df.groupby('co2'):
    ax.plot(sdf['temp'], sdf['amax'], 'o-', label=f"{co2:.0f} [µmol mol-1]")

ax.legend(loc='upper right')
x = np.linspace(18, 40, 100)
ax.plot(x, crv(x, opt_amax), '-')
ax.text(22, 5, f"amax={opt_amax[0]:.2f} {opt_amax[1]:+.2f}*temp {opt_amax[2]:+.2f}*temp^2")
ax.set_ylim(0, 25)
ax.set_ylabel("A_max [µmolC m-2 s-1]")

ax = axes[1, 0]
for co2, sdf in df.groupby('co2'):
    ax.plot(sdf['temp'], sdf['ppfd_sat'], 'o-', label=f"{co2:.0f}")
x = np.linspace(18, 40, 100)
ax.plot(x, crv(x, opt_ppfd_sat), '-')
ax.text(20, 500, f"ppfd_sat={opt_ppfd_sat[0]:.2f} {opt_ppfd_sat[1]:+.2f}*temp {opt_ppfd_sat[2]:+.2f}*temp^2")
ax.set_xlim(18, 40)
ax.set_ylim(0, 1800)
ax.set_xlabel("Leaf temperature [°C]")
ax.set_ylabel("ppfd_sat [µmolphoton m-2 s-1]")

fig.tight_layout()
plt.show()
