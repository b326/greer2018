"""
Assimilation light response curves
==================================

Plot Assimilation light response curves to compare to fig2.
"""
import matplotlib.pyplot as plt
import pandas as pd

from greer2018 import pth_clean

# read data
df = pd.read_csv(pth_clean / "net_photosynthesis.csv", sep=";", comment="#")

# TODO single CO2 concentration for the moment

# plot data
fig, axes = plt.subplots(1, 1, figsize=(10, 6), squeeze=False)

ax = axes[0, 0]
for temp, sdf in df.groupby(by='temp'):
    ax.plot(sdf['ppfd'], sdf['photo_net'], 'o-', label=f"{temp:d}°C")

ax.legend(loc='lower right')
ax.set_ylim(-5, 15)
ax.set_xlabel("PPFD [µmolphoton m-2 s-1]")
ax.set_ylabel("Net Photosynthesis [µmolC m-2 s-1]")

fig.tight_layout()
plt.show()
